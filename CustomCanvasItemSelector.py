"""Rect Tracker class for Python Tkinter Canvas"""

def groups(glist, numPerGroup=2):
	result = []

	i = 0
	cur = []
	for item in glist:
		if not i < numPerGroup:
			result.append(cur)
			cur = []
			i = 0

		cur.append(item)
		i += 1

	if cur:
		result.append(cur)

	return result

def average(points):
	aver = [0,0]
	
	for point in points:
		aver[0] += point[0]
		aver[1] += point[1]
		
	return aver[0]/len(points), aver[1]/len(points)

class Selector:
	
	def __init__(self, canvas, data):
		self.canvas = canvas
		self.item = None
		self.b1 = None
		self.b1m = None
		self.b1r = None
		self.b1c = None
		self.b1mc = None
		self.b1rc = None
		self.data = data
		self.doIt = None
		
	def draw(self, start, end, **opts):
		"""Draw the rectangle"""
		return self.canvas.create_rectangle(*(list(start)+list(end)), **opts)
		
	def autodraw(self, **opts):
		"""Setup automatic drawing; supports command option"""
		self.start = None
		self.b1 = self.canvas.bind("<Button-1>", self.__update, '+')
		self.b1m = self.canvas.bind("<B1-Motion>", self.__update, '+')
		self.b1r = self.canvas.bind("<ButtonRelease-1>", self.__stop, '+')
		
		self.b1c = self.canvas.bind("<Control-1>", self.__updateBis, '+')
		self.b1mc = self.canvas.bind("<Control-B1-Motion>", self.__updateBis, '+')
		self.b1rc = self.canvas.bind("<Control-ButtonRelease-1>", self.__stopBis, '+')
		self._command = opts.pop('command', lambda *args: None)
		self.doIt = opts.pop('autoUpdate', True)
		self.rectopts = opts
		
	def stopAutodraw(self):
		"""Setup automatic drawing; supports command option"""
		self.start = None
		self.canvas.unbind("<Button-1>", self.b1)
		self.canvas.unbind("<B1-Motion>", self.b1m)
		self.canvas.unbind("<ButtonRelease-1>", self.b1r)
		self.canvas.unbind("<Control-1>", self.b1c)
		self.canvas.unbind("<Control-B1-Motion>", self.b1mc)
		self.canvas.unbind("<Control-ButtonRelease-1>", self.b1rc)
		self.b1 = None
		self.b1m = None
		self.b1r = None
		self.b1c = None
		self.b1mc = None
		self.b1rc = None
		self._command = None
		self.rectopts = None
		
	def __update(self, event):
		if not self.start:
			self.start = [self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)]
			return
		
		if self.item is not None:
			self.canvas.delete(self.item)
		self.item = self.draw(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)), **self.rectopts)
		if self.doIt :
		    self._command(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)))
		
	def __updateBis(self, event):
		if not self.start:
			self.start = [self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)]
			return
		if self.item is not None:
			self.canvas.delete(self.item)
		self.item = self.draw(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)), **self.rectopts)
		if self.doIt :
		    self._command(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)),True,False)
		
	def __stop(self, event):
	    if not self.doIt :
	        self._command(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)))
	    self.start = None
	    self.canvas.delete(self.item)
	    self.item = None
	    if self.doIt :
	        self.data.Button1.invoke()
		    
		
	def __stopBis(self, event):
	    if not self.doIt :
	        self._command(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)))
	    if self.item is not None:
		    self.canvas.delete(self.item)
	    self._command(self.start, (self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)),True,True)
	    self.start = None
	    self.canvas.delete(self.item)
	    self.item = None
	    if self.doIt :
	        self.data.selectionModeButton.invoke()
	
	def hit_test(self, start, end, tags=None, ignoretags=None, ignore=[]):
		"""
		Check to see if there are items between the start and end
		"""
		ignore = set(ignore)
		ignore.update([self.item])
		
		# first filter all of the items in the canvas
		if isinstance(tags, str):
			tags = [tags]
		
		if tags:
			tocheck = []
			for tag in tags:
				tocheck.extend(self.canvas.find_withtag(tag))
		else:
			tocheck = self.canvas.find_all()
		tocheck = [x for x in tocheck if x != self.item]
		if ignoretags:
			if not hasattr(ignoretags, '__iter__'):
				ignoretags = [ignoretags]
			tocheck = [x for x in tocheck if x not in self.canvas.find_withtag(it) for it in ignoretags]
		
		self.items = tocheck
		
		# then figure out the box
		xlow = min(start[0], end[0])
		xhigh = max(start[0], end[0])
		
		ylow = min(start[1], end[1])
		yhigh = max(start[1], end[1])
		
		items = []
		for item in tocheck:
			if item not in ignore:
				x, y = average(groups(self.canvas.coords(item)))
				if (xlow < x < xhigh) and (ylow < y < yhigh):
					items.append(item)
	
		return items
		
