from __future__ import division
from matplotlib import pyplot as plt
import tkinter as tk
from tkinter.colorchooser import askcolor
import tkinter.messagebox as tkMessageBox
import tkinter.filedialog as tkFileDialog
import tkinter.simpledialog as tkSimpleDialog
import tkinter.ttk as ttk
from pathlib import Path
import VideoFrameManager
import AnnotationUICentralFrame
import DialogBoxFactory
import ObjectListManager
import CustomCanvasObjects
import sys, os, os.path, argparse
import random, numpy
from trafficintelligence import storage, moving
from PIL import ImageTk, Image
import copy


#random number between 0 and 255
r = lambda: random.randint(0,255)
deltax, deltay = 300, 130 #deltay : space for menubar, and control buttons ; deltay :  space for list
__version__ = '1.0'

#Main frame class
class VideoTool:
    def __init__(self, window, window_title, configFilename = None, mode='Object', width = 600, height = 800, thickness = 5, deform = False, databaseFilename = None, videoFilename = None, homographyFilename = None, frameJump = 5 ):
        self.window = window
        if window.winfo_screenwidth()<width + deltax +10 or height + deltay +10 > window.winfo_screenheight():
            print ('Your screen is not large enough')
            tkMessageBox.showerror("Screen Error", "Your screen is not large enough")
            sys.exit()
        self.window.geometry('{}x{}+0+0'.format(width + deltax, height + deltay))
        try :
            self.window.attributes('-zoomed', False)
        except :
            pass
        self.window.title(window_title)
        self.frameJump = max(frameJump, 1)
        self.deform = deform
        #self.params = storage.ProcessParameters()
        self.objectList=ObjectListManager.List()
        self.playStatus=False
        self.fullscreen=False
        self.ObjectsColor=[]
        self.Points=[]
        self.invHomography=None
        self.Data=[]
        self.VideoFrameManager= None
        self.mode= 'Object'
        self.frameNumber = -1
        self.PlotThickness = thickness
        self.PanedWindow = tk.PanedWindow(self.window, orient = tk.HORIZONTAL)
        self.PanedWindow.pack(expand=tk.TRUE, fill=tk.BOTH, pady=2, padx=2)
        pathname = os.path.dirname(sys.argv[0])+('/' if len(os.path.dirname(sys.argv[0]))>0 else '')
        self.lastDimensions = None;
        self.currendWorkingDirectory = os.getcwd()
        
        ##Video management
        self.videoTopLabeledFrame = tk.LabelFrame(self.PanedWindow, borderwidth=2,text="Video", relief=tk.GROOVE)
        self.CentralPanedWindow = tk.PanedWindow(self.videoTopLabeledFrame, orient = tk.VERTICAL)
        self.CentralPanedWindow.pack(expand=tk.TRUE, fill=tk.BOTH, pady=2, padx=2)
        self.ZoomableMasterFrame = tk.Frame(self.CentralPanedWindow, width=width, height=height, bg = 'yellow')
        self.Zoomable = AnnotationUICentralFrame.ZoomableFrame(mainframe = self.ZoomableMasterFrame, data=self, width = width, height = height)
        self.Zoomable.pack(expand=tk.TRUE,fill=tk.BOTH)
        VideoCommandFrame = tk.Frame(self.CentralPanedWindow)
        SliderFrame = tk.Frame(VideoCommandFrame)
        self.slider = tk.Scale(SliderFrame, orient=tk.HORIZONTAL, tickinterval=10)
        self.slider.pack(expand=tk.TRUE,fill=tk.BOTH)
        SliderFrame.pack(side = tk.TOP, expand=tk.TRUE,fill=tk.BOTH)
        ButtonsFrame = tk.Frame(VideoCommandFrame)
        imgPrev = ImageTk.PhotoImage(Image.open(pathname+"Images/previous-icon.png"))  
        self.previousButton = tk.Button(ButtonsFrame, image=imgPrev, command=self.readPreviousFrame, relief=tk.FLAT )
        self.previousButton.pack(side=tk.LEFT)
        tk.Button(ButtonsFrame, text="Load a video", command=self.LoadVideo, relief=tk.GROOVE).pack(side=tk.LEFT)
        imgPlay = ImageTk.PhotoImage(Image.open(pathname+"Images/play-icon.png"))
        tk.Button(ButtonsFrame, image=imgPlay, command= self.togglePlayButton, relief=tk.FLAT).pack(side=tk.LEFT)
        tk.Button(ButtonsFrame, text="Load a data base file", command= self.LoadDatabase, relief=tk.GROOVE).pack(side=tk.LEFT)
        imgNext = ImageTk.PhotoImage(Image.open(pathname+"Images/next-icon.png")) 
        self.nextButton = tk.Button(ButtonsFrame, image=imgNext, command= self.readFrame, relief=tk.FLAT)
        self.nextButton.pack(side=tk.LEFT)
        self.nextButton.bind('<Button-3>', self.onNextFrameButtonDoubleClick)
        ButtonsFrame.pack(side = tk.TOP)
        self.tracker = None
        self.trackerBounding = None
        
        ##trajectories management
        ListLabeledFrame = tk.LabelFrame(self.PanedWindow, borderwidth=2,text="Options", relief=tk.GROOVE)
        self.Notebook = ttk.Notebook(ListLabeledFrame)
        DynamicListFrame = ttk.Frame(self.Notebook)   
        Listscrollbar = tk.Scrollbar(DynamicListFrame,orient="vertical")
        self.dynamicListBox = tk.Listbox(DynamicListFrame,selectmode=tk.EXTENDED,yscrollcommand=Listscrollbar.set)
        self.dynamicListBox.pack(fill=tk.BOTH,side = tk.LEFT,expand=tk.TRUE)
        Listscrollbar.configure( command = self.dynamicListBox.yview )
        Listscrollbar.pack( side = tk.RIGHT, fill = tk.Y,expand=tk.FALSE )
        DynamicListFrame.pack(fill = tk.BOTH,expand=tk.TRUE)
        ListFrame = tk.Frame(self.Notebook)
        Listscrollbar = tk.Scrollbar(ListFrame, orient = "vertical")
        self.staticListBox = tk.Listbox(ListFrame, selectmode = tk.EXTENDED, yscrollcommand = Listscrollbar.set)
        self.staticListBox.pack(fill=tk.BOTH,side = tk.LEFT,expand=tk.TRUE)
        Listscrollbar.configure( command = self.staticListBox.yview )
        Listscrollbar.pack( side = tk.RIGHT, fill = tk.Y,expand=tk.FALSE )
        self.staticListBox.bind('<Button-2>',self.B2MouseClick)
        self.staticListBox.bind('<Button-3>', self.B3MouseClick)
        self.staticListBox.bind("<MouseWheel>", self.ListScroll)
        self.staticListBox.bind('<Button-5>',   self.ListScroll)  # only with Linux, wheel scroll down
        self.staticListBox.bind('<Button-4>',   self.ListScroll)  # only with Linux, wheel scroll up
        self.staticListBox.bind('<<ListboxSelect>>', self.ListSelection)
        self.dynamicListBox.bind('<Button-2>',self.B2MouseClick)
        self.dynamicListBox.bind('<Button-3>',self.B3MouseClick)
        self.dynamicListBox.bind("<MouseWheel>", self.ListScroll)
        self.dynamicListBox.bind('<Button-5>',   self.ListScroll)
        self.dynamicListBox.bind('<Button-4>',   self.ListScroll)
        self.dynamicListBox.bind('<<ListboxSelect>>', self.ListSelection)
        ListFrame.pack(fill = tk.BOTH,expand=tk.TRUE)
        self.Notebook.add(ListFrame, text='Static list')
        self.Notebook.add(DynamicListFrame, text='Dynamic list')
        self.Notebook.pack(fill = tk.BOTH,expand=tk.TRUE)
        self.Button1 = tk.Button(ListLabeledFrame, command = self.PushB1, borderwidth=2)
        self.Button1.pack(fill = tk.X, pady= 2)
        self.Button2 = tk.Button(ListLabeledFrame, borderwidth=2)
        self.Button2.pack(fill = tk.X, pady= 2)
        self.Button3=tk.Button(ListLabeledFrame, borderwidth=2)
        self.Button3.pack(fill = tk.X, pady= 2)
        self.ModeLabel = tk.Label(ListLabeledFrame, text="You are in "+self.mode+' mode')
        self.ModeLabel.pack(fill = tk.X, pady= 2)
        self.HomeButton = tk.Button(ListLabeledFrame, text="Go back to object Mode", command = self.ObjectModeLoader, borderwidth=2 )
        self.HomeButton.pack(fill = tk.X, pady= 2)
        tk.Button(ListLabeledFrame, 
            text="Select none", 
            command= self.selectNoneOfTheListItems, borderwidth=2).pack(side = tk.LEFT,expand=tk.TRUE,fill = tk.X, pady= 2)
        tk.Button(ListLabeledFrame, 
            text="Select all", 
            command= self.selectAllOfTheListItems, borderwidth=2).pack(side = tk.LEFT,expand=tk.TRUE,fill = tk.X, pady= 2)
        self.MenuBarConfigure()
        self.PanedWindow.add(ListLabeledFrame, minsize = deltax)
        self.PanedWindow.add(self.videoTopLabeledFrame, width = width)
        self.CentralPanedWindow.add(self.ZoomableMasterFrame, height = height)
        self.CentralPanedWindow.add(VideoCommandFrame, minsize = 90)
        
        #signals'handling
        self.Notebook.bind('<<NotebookTabChanged>>', self.ListSynchronize)
        self.window.bind('<Control-KeyPress-f>', self.ToggleFullScreen)
        self.window.bind('<Control-KeyPress-F>', self.ToggleFullScreen)
        self.window.bind('<Escape>', self.OutFullScreen)  
        self.window.bind('<Control-KeyPress-v>', self.LoadVideo)
        self.window.bind('<Control-KeyPress-V>', self.LoadVideo)
        self.window.bind('<Control-KeyPress-D>', self.LoadDatabase)
        self.window.bind('<Control-KeyPress-d>', self.LoadDatabase)
        self.window.bind('<Control-KeyPress-c>', self.LoadConfigurationFile)
        self.window.bind('<Control-KeyPress-C>', self.LoadConfigurationFile)
        self.window.bind('<Control-KeyPress-S>', self.SaveDatabase)
        self.window.bind('<Control-KeyPress-s>', self.SaveDatabase)
        self.window.bind('<Control-KeyPress-q>', self.DeleteWindow)
        self.window.bind('<Control-KeyPress-Q>', self.DeleteWindow)
        self.window.bind('<KeyPress-Up>',   self.Zoomable.scroolImage)
        self.window.bind('<KeyPress-Down>',   self.Zoomable.scroolImage)
        self.window.bind('<KeyPress-Left>',   self.Zoomable.scroolImage)
        self.window.bind('<KeyPress-Right>',   self.Zoomable.scroolImage)
        self.window.bind('<Control-KeyPress-a>', self.selectAllOfTheListItems)
        self.window.bind('<Control-KeyPress-A>', self.selectAllOfTheListItems)
        self.window.bind('<Control-KeyPress-z>', self.ObjectListUndo)
        self.window.bind('<Control-KeyPress-Z>', self.ObjectListUndo)
        self.window.bind('<Control-KeyPress-y>', self.ObjectListRedo)
        self.window.bind('<Control-KeyPress-Y>', self.ObjectListRedo)
        
        #starting Program
        # TODO rewrite: use storage.processVideoArguments(args) to get parameters, then, like displayTrajectories, load from command line options if provided or, last option, from menu box
        if configFilename is not None and len(configFilename)>0:
            self.readConfigurationFile(configFilename, databaseFilename, videoFilename, homographyFilename)
        elif videoFilename is not None and len(videoFilename)>0:
            self.readVideoFile(videoFilename, databaseFilename, homographyFilename)
        else: # if not hasattr(self.params,'videoFilename'):
            d =  DialogBoxFactory.ChoiceDialog(self.window,
                     text='The type of file', choices=['configuration file' , 'video file'], 
                     title='Configure the video source', CancelCode = None)
            res = d.go()
            if res =='video file':
                self.LoadVideo(databaseFilename = databaseFilename, homographyFilename = homographyFilename)
            elif res is not None:
                self.LoadConfigurationFile(databaseFilename = databaseFilename, homographyFilename = homographyFilename)
        if not hasattr(self,'params'):#hasattr(self.params,'videoFilename'):
            print ('You need a video at least')
            tkMessageBox.showerror("Loading error", "You need a video at least", parent = self.window)
            sys.exit()
        self.SliderConfigurator()
        self.ModeUpdateManager()
        self.play()
        self.window.protocol('WM_DELETE_WINDOW', self.DeleteWindow)
        self.window.mainloop()
    
    #Interface Builders
    
    #Changes the slider limits
    def SliderConfigurator(self, LowestValue=None, HighestValue=None):
        if LowestValue is None:
            LowestValue=0
        if HighestValue is None and hasattr(self.params,'videoFilename'):
            HighestValue=self.VideoFrameManager.get_max_frame_pos()
        elif HighestValue is not None:
            HighestValue=min(HighestValue,self.VideoFrameManager.get_max_frame_pos())
        else:
            HighestValue = 0
        LowestValue, HighestValue = min(LowestValue, HighestValue), max(LowestValue, HighestValue)
        if self.VideoFrameManager.is_frame_pos_limited():
            self.slider.config(tickinterval=int((HighestValue-LowestValue)/10.0), from_=LowestValue, to=HighestValue)
            def tmp(event):
                self.playStatus = False
            self.slider.bind('<ButtonRelease-1>', self.SetSliderPosition, '+')
            self.slider.bind('<Button-1>', tmp, '+')
            self.slider.set(LowestValue)
        else:
            self.slider.config(from_=-1, to=-1, tickinterval=1)
            self.slider.unbind('<ButtonRelease-1>')
            self.slider.unbind('<Button-1>')
            
    #Builds the menu bar
    def MenuBarConfigure(self):
        menubar = tk.Menu(self.window)
        # create a pulldown menu, and add it to the menu bar
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open a video", command=self.LoadVideo,accelerator='Ctrl+V')
        filemenu.add_command(label="Open a data base file", command=self.LoadDatabase,accelerator='Ctrl+D')
        filemenu.add_command(label="Open a configuration file", command=self.LoadConfigurationFile, accelerator='Ctrl+C')
        
        editionmenu = tk.Menu(menubar, tearoff=0)
        editionmenu.add_command(label="Select all the items", command=self.selectAllOfTheListItems, accelerator='Ctrl+A')
        editionmenu.add_separator()
        
        submenu = tk.Menu(editionmenu, tearoff=0)
        editionmenu.add_cascade(label="Build an object", menu=submenu)
        submenu.add_command(label="using a point", command=self.EditionModeLoader)
        submenu.add_command(label="using a box", command=self.EditionModeLoader2)
        editionmenu.add_command(label="Track an object" ,command=self.TrackerModeLoader)
        editionmenu.add_separator()
        editionmenu.add_command(label="Undo", command=self.ObjectListUndo, accelerator='Ctrl+Z')
        editionmenu.add_command(label="Redo", command=self.ObjectListRedo, accelerator='Ctrl+Y')
        filemenu.add_separator()
        filemenu.add_command(label="Save your objects in a data base file",accelerator='Ctrl+S',command=self.SaveDatabase)
        filemenu.add_command(label="Quit", command=self.DeleteWindow, accelerator='Ctrl+Q')
        menubar.add_cascade(label="File", menu=filemenu)
        menubar.add_cascade(label="Edition", menu=editionmenu)
        menubar.add_command(label="About", command=self.showAbout)
        # display the menu
        self.window.config(menu=menubar)
       
    #Mouse Central Button popup menu builder
    def B2MouseMenuContent(self, event):
        self.playStatus=False
        popup = tk.Menu(self.window, tearoff=0)
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            
            popup.add_command(label="Objects Management")
            popup.add_separator()
            submenu = tk.Menu(popup, tearoff=0)
            popup.add_cascade(label="Build an object", menu=submenu)
            submenu.add_command(label="using a point", command=self.EditionModeLoader)
            submenu.add_command(label="using a box", command=self.EditionModeLoader2)
            popup.add_command(label="Manage all the features" ,command=self.FeatureManagementModeLoader())
            popup.add_command(label="Track an object" ,command=self.TrackerModeLoader)
            popup.add_command(label="Undo" ,command=self.ObjectListUndo)
            popup.add_command(label="Redo" ,command=self.ObjectListRedo)
            if not self.objectList.can_undo():
                popup.entryconfig(5, state=tk.DISABLED)
            if not self.objectList.can_redo():
                popup.entryconfig(6, state=tk.DISABLED)
        elif self.mode=="Feature":
            popup.add_command(label="Features positions management")
            popup.add_separator()
            popup.add_command(label="Update the list labels", command = self.showVisibilitiAndSaveStatutForListItems )
            popup.add_command(label="Go back to Object mode", command = self.ObjectModeLoader )
        elif self.mode=="feature management":
            popup.add_command(label="Features management")
            popup.add_separator()
            popup.add_command(label="Go back to Object mode", command = self.ObjectModeLoader )
        elif self.mode.startswith( 'Object creation' ):
            popup.add_command(label="Object creation Management")
            popup.add_separator()
            popup.add_command(label="Go back to Object mode", command = self.ObjectModeLoader )
        try:
            popup.tk_popup(event.x_root, event.y_root, 0)
        finally:
            # make sure to release the grab (Tk 8.0a1 only)
            popup.grab_release()
    
    #Mouse Central Button popup menu builder
    def B3MouseMenuContent(self, event):
        self.playStatus=False
        fromstatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        idx = list((self.staticListBox.curselection() if fromstatic else self.dynamicListBox.curselection()))
        if not fromstatic:
            idx = [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in idx]
        if len(idx) == 0:
            self.B2MouseMenuContent(event)
        elif len(idx) == 1:
            popup = tk.Menu(self.window, tearoff=0)
            if self.mode=='Feature':
                popup.add_command(label="You have selected the feature #"+str(self.Points[idx[0]].idtext))
                popup.add_separator()
                popup.add_command(label="Set background color",command = self.SetListItemColor(idx[0]) )
                popup.add_command(label="Set foreground color",command = self.SetListItemColor(idx[0],'fg') )
                popup.add_command(label="Set the selected feature "+('in' if self.Points[idx[0]].getVisibility() else '')+'visible',command = self.setAnObjectVisibility([idx[0]],not self.Points[idx[0]].getVisibility()) )
                popup.add_command(label="Undo",command = self.Points[idx[0]].undoPosition )
                popup.add_command(label="Redo",command = self.Points[idx[0]].redoPosition )
                popup.add_separator()
                popup.add_command(label="Update the list labels",command = self.showVisibilitiAndSaveStatutForListItems )
                popup.add_command(label="Select the point", command = self.ListSelection)
                
                if not self.Points[idx[0]].canUndoPosition():
                    popup.entryconfig(5, state=tk.DISABLED)
                if not self.Points[idx[0]].canRedoPosition():
                    popup.entryconfig(6, state=tk.DISABLED) 
            elif self.mode=='Object' or self.mode.startswith( 'Examination' ):
                popup.add_command(label="You have selected the object #"+str(self.objectList[idx[0]].getNum()))
                popup.add_separator()
                popup.add_command(label="Crop the feature", command = self.CropObjectToNow(idx[0]))
                popup.add_command(label="Set the feature StartFrame", command = self.CropObjectTimeInterval(idx[0]))
                popup.add_command(label="Set the feature EndFrame", command = self.CropObjectTimeInterval(idx[0], begin = False))
                popup.add_separator()
                popup.add_command(label="Split the feature", command = self.splitObjectAtNow(idx[0]))
                popup.add_command(label="Split at a specific instant", command = self.splitObjectUI(idx[0]))
                popup.add_separator()
                popup.add_command(label="Set background color",command = self.SetListItemColor(idx[0]) )
                popup.add_command(label="Set foreground color",command = self.SetListItemColor(idx[0],'fg') )
                popup.add_command(label="Manage its features positions" ,command=self.FeatureModeLoader)
                popup.add_command(label="Manage its features" ,command=self.FeatureManagementModeLoader([idx[0]]))
                popup.add_command(label="Set the slider to the object time interval" ,command=self.setSliderObjectTimeInterval(idx[0]))
                popup.add_command(label="Set the object type",command = self.SetObjectType(idx[0]) )
                popup.add_command(label="Set the number of objects", command = self.SetObjectNObjects(idx[0]) )
                if not self.CanCropToNow(idx[0]):
                    popup.entryconfig(2, state=tk.DISABLED)
                if not self.CanSplitAtNow(idx[0]):
                    popup.entryconfig(6, state=tk.DISABLED)
                
            elif self.mode=="feature management":
                popup.add_command(label="You have selected the feature #"+str(self.Points[idx[0]].idtext))
                popup.add_separator()
                popup.add_command(label="Set background color",command = self.SetListItemColor(idx[0]) )
                popup.add_command(label="Set foreground color",command = self.SetListItemColor(idx[0],'fg') )
                popup.add_command(label="Set the selected feature "+('in' if self.Points[idx[0]].getVisibility() else '')+'visible',command = self.setAnObjectVisibility([idx[0]],not self.Points[idx[0]].getVisibility()) )
            elif self.mode.startswith( 'Object creation' ):
                popup.add_command(label="Object creation management")
                popup.add_separator()
                popup.add_command(label="Set background color",command = self.SetListItemColor(idx[0]) )
                popup.add_command(label="Set foreground color",command = self.SetListItemColor(idx[0],'fg') )
                popup.add_command(label="Undo",command = self.Points[idx[0]].undoPosition )
                popup.add_command(label="Redo",command = self.Points[idx[0]].redoPosition ) 
                if not self.Points[idx[0]].canUndoPosition():
                    popup.entryconfig(5, state=tk.DISABLED)
                if not self.Points[idx[0]].canRedoPosition():
                    popup.entryconfig(6, state=tk.DISABLED)
                popup.add_separator()
                popup.add_command(label="Creation in progess")
            
            try:
                popup.tk_popup(event.x_root, event.y_root, 0)
            finally:
                # make sure to release the grab (Tk 8.0a1 only)
                popup.grab_release()
        else :
            popup = tk.Menu(self.window, tearoff=0)
            if self.mode=='Feature':
                popup.add_command(label="You have selected "+str(len(list((self.staticListBox.curselection() if fromstatic else self.dynamicListBox.curselection()))))+" Features")
                popup.add_separator()
                popup.add_command(label="Set the selected features visible",command = self.setAnObjectVisibility(idx, True) )
                popup.add_command(label="Set the selected features invisible",command = self.setAnObjectVisibility(idx,False) )
                popup.add_separator()
                popup.add_command(label="Update the list labels",command = self.showVisibilitiAndSaveStatutForListItems )
            elif self.mode=='feature management':
                popup.add_command(label="You have selected "+str(len(idx))+" Features")
                popup.add_separator()
                popup.add_command(label="Set the selected features visible",command = self.setAnObjectVisibility(idx,True ) )
                popup.add_command(label="Set the selected features invisible ",command = self.setAnObjectVisibility(idx,False) )
            elif self.mode=='Object' or self.mode.startswith( 'Examination' ):
                popup.add_command(label="You have selected "+str(len(idx))+" Objects")
                popup.add_separator()
                popup.add_command(label="Manage their features positions" ,command=self.FeatureModeLoader)
                popup.add_command(label="Manage their features" ,command=self.FeatureManagementModeLoader(idx))
                
            try:
                popup.tk_popup(event.x_root, event.y_root, 0)
            finally:
                # make sure to release the grab (Tk 8.0a1 only)
                popup.grab_release()
                
    #############################################################Signal Handler
    
    #Signal Handler
    '''To add a shortcut : in the constructor add
            self.window.bind(event , handler)
    '''
    #When you close the window : ShortCut : Ctrl-Q
    def DeleteWindow(self, event = None):
        if self.saveStatus() and not tkMessageBox.askyesno("Save!","Do you want to ignore your last modifications ?"):
            return
        self.window.quit()
        
    #When set color is chosen from popup menu
    #This function returns a function allowing to change the color depending on the item id
    def SetListItemColor(self, idx, BgOrFg='bg'):
        def tmp() :
            color = askcolor(parent = self.window, title = 'Choose a color', initialcolor = self.Points[idx].color if BgOrFg=='bg' else self.Points[idx].textcolor)
            if color[1] is not None: 
                self.staticListBox.itemconfig(idx, {BgOrFg:color[1]})
                self.Points[idx].setColor(color[1], BgOrFg=='bg')
                if self.mode=='Object' or self.mode.startswith( 'Examination' ):
                    self.ObjectsColor[idx] = (color[1],self.ObjectsColor[idx][1]) if BgOrFg=='bg' else (self.ObjectsColor[idx][0],color[1])
                self.readFrame(Next = False)
        return tmp
        
    #SelectionButton Handler
    def PushB1(self, event=None):
        if self.Button1['text']=='Enable selection mode':
            self.Zoomable.setSelectionMode(True)
            self.Button1.config(text='Disable selection mode')
        elif self.Button1['text']=='Disable selection mode':
            self.Zoomable.setSelectionMode(False)
            self.Button1.config(text='Enable selection mode')
        elif self.Button1['text']== 'Delete and re-bound the tracker':
            if self.trackerBounding is not None:
                self.trackerBounding.deleteFromCanvas()
            self.trackerBounding = None
            self.Zoomable.setInitTracker()
            
      
    #Central mouse button click. It clears the selection of the list and calls B2MouseMenuContent
    def B2MouseClick(self,event):
        self.selectNoneOfTheListItems()
        self.B2MouseMenuContent(event)
    
    #Right mouse button click. It selects the nearest objects a
    def B3MouseClick(self,event):
        fromstatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        if len(list((self.staticListBox.curselection() if fromstatic else self.dynamicListBox.curselection())))<2:
            idx = event.widget.nearest(event.y)
            if not fromstatic:
                idx = self.staticListBox.get(0, tk.END).index(self.staticListBox.get(idx))
            event.widget.selection_clear(0,event.widget.size())
            if idx!=-1:
                event.widget.selection_set(idx)
        self.B3MouseMenuContent(event)
        
    #When scrolling the list with the mouse
    def ListScroll(self, event):
        if event.num == 5 or event.delta == -120:
            sens=1
        if event.num == 4 or event.delta == 120:
            sens=-1
        event.widget.yview_scroll(sens, "units")
            
    #When changing the current tab in the note Pad
    def ListSynchronize(self, event = None):
        tostatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        if tostatic : 
            self.staticListBox.unbind('<<ListboxSelect>>')
            self.staticListBox.selection_clear(0, self.staticListBox.size())
            for i in self.dynamicListBox.curselection() :
                self.staticListBox.selection_set(list(self.staticListBox.get(0, tk.END)).index(self.dynamicListBox.get(i)))
            self.staticListBox.bind('<<ListboxSelect>>', self.ListSelection)
            if self.mode=='Feature' or self.mode=="Aggreation de feature":
                self.SelectPoints([self.Points[i].OvalItem for i in self.staticListBox.curselection()])
        else :
            self.dynamicListBox.unbind('<<ListboxSelect>>')
            self.dynamicListBox.selection_clear(0, self.dynamicListBox.size())
            for i in self.staticListBox.curselection() :
                if self.staticListBox.get(i) in self.dynamicListBox.get(0, tk.END):
                    self.dynamicListBox.selection_set(self.dynamicListBox.get(0, tk.END).index(self.staticListBox.get(i)))
            self.dynamicListBox.bind('<<ListboxSelect>>', self.ListSelection)
            if self.mode=='Feature' or self.mode=="Aggreation de feature":
                self.SelectPoints([self.Points[list(self.staticListBox.get(0, tk.END)).index(self.dynamicListBox.get(i))].OvalItem for i in list(self.dynamicListBox.curselection())])
    
    #when selecting items from the list
    def ListSelection(self, event=None):
        fromstatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        l = [(self.Points[i].OvalItem if fromstatic else self.Points[self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i))].OvalItem) for i in (self.staticListBox.curselection() if fromstatic else self.dynamicListBox.curselection())]
        self.SelectPoints(l)      
        
    #Switching between fullscreen and not ; shortcut : CTRL-F
    def ToggleFullScreen(self,event=None):
        self.fullscreen=not self.fullscreen
        self.window.attributes('-fullscreen', self.fullscreen)
    
    #Going out of fullscreen ; shortcut : esc
    def OutFullScreen(self,event=None):
        self.fullscreen=False
        self.window.attributes('-fullscreen', self.fullscreen) 

    #When click on play button
    def togglePlayButton(self):
        self.playStatus = not self.playStatus
             
    #Auto-Play
    def play(self):
        if self.playStatus:
            self.readFrame(setOnPlay=True)
        #wait and load next frame
        self.window.after(self.VideoFrameManager.get_delay_between_frame(), self.play)
        
     
    #Clears the selection       
    def selectNoneOfTheListItems(self):
        self.SelectPoints([])
        
    #Selects all the items ; shortcut : CTRL-A
    def selectAllOfTheListItems(self, event = None):
        fromstatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        l = [(self.Points[i].OvalItem if fromstatic else self.Points[self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i))].OvalItem) for i in (range(self.staticListBox.size()) if fromstatic else range(self.dynamicListBox.size()))]
        self.SelectPoints(l)
        
    #When using the slider
    def SetSliderPosition(self, event = None):
        if  self.frameNumber-1 != int(self.slider.get()) :
            #self.playStatus=True
            self.frameNumber = int(self.slider.get())+1
            self.VideoFrameManager.set_to_specific_frame(int(self.slider.get())+1)
            self.readFrame(Next=False)
     
    #Saving features positions
    def saveFeatures(self ):
        if self.mode=='Feature':
            liste = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
            if liste == [] :
                liste = self.Points
            else:
                liste = [self.Points[i] for i in liste]
            if self.saveStatus(liste):
                if tkMessageBox.askyesno("Creating a local savepoint","Do you want to erase selected features savepoints (Irreversible) ? "+("(all of them)" if liste is None else "")):
                    for p in liste:
                        p.saveData()
                    for obj in list(set([p.obj for p in liste])):
                        if hasattr(obj,'projectedPositions'):
                            delattr(obj,'projectedPositions')
                        obj.updatePositions()
            self.showVisibilitiAndSaveStatutForListItems()
    
    #Restoring features to the last save
    def restoreFeatures(self):
        if self.mode=='Feature':
            liste = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
            if liste == [] :
                liste = None
            else:
                liste = [self.Points[i] for i in liste]
            if self.saveStatus(liste):
                if tkMessageBox.askyesno("Jump to the last local savepoint","You will loose your unsaved modifications (Irreversible)!"+'(all of them)' if liste is None else ''+'\nDo you want to continue ? '):
                    for p in liste:
                        p.restoreData()
                    for obj in list(set([p.obj for p in liste])):
                        if hasattr(obj,'projectedPositions'):
                            delattr(obj,'projectedPositions')
                        obj.updatePositions()
            self.showVisibilitiAndSaveStatutForListItems()
       
    #About dialog  
    def showAbout(self,event=None):
        d = DialogBoxFactory.SimpleDialog(self.window,
                         text="This is a Tool for the Traffic Intelligence project. This software project provides a set of tools developed by Nicolas Saunier and his collaborators for transportation data processing, in particular road traffic, motorized and non-motorized. The project consists in particular in tools for the most typical transportation data type, trajectories, i.e. temporal series of positions. The original work targeted automated road safety analysis using video sensors." ,
                         buttons=["Ok"],
                         default=0,
                         cancel=2,
                         title="About the app",imagePath="Images/about.jpeg")
        d.go()
    
    #Undo for objectList ; shortcut : CTRL-Z
    def ObjectListUndo(self, event = None):
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            if self.objectList.can_undo():
                self.objectList.undo()
                self.ModeUpdateManager()
        else:
            tkMessageBox.showerror("Forbidden", 'Must go back to object mode first')
      
    #Redo for objectList ; shortcut : CTRL-Y
    def ObjectListRedo(self, event = None):
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            if self.objectList.can_redo():
                self.objectList.redo()
                self.ModeUpdateManager()
        else:
            tkMessageBox.showerror("Forbidden", 'Must go back to object mode first')
            
    
    ######################################################Signal Handler
    
    #DataBase management
    
    def LoadDatabase(self,event=None):
        self.playStatus=False
        if self.saveStatus() and not tkMessageBox.askyesno("Save!","Do you want to ignore your last modifications ?"):
            return
        filename=tkFileDialog.askopenfilename(initialdir = self.currendWorkingDirectory,title = "Loading the database",filetypes = [("SQLite","*.sqlite")])
        if len(filename)>0 :
            self.currendWorkingDirectory = os.path.dirname(filename)
            d = DialogBoxFactory.ProgressBarBox(self.window, title = 'Loading Database file', text = 'Loading..', determinateMode = 0, finishText = 'Done')
            d.go()
            try:
                data = storage.loadTrajectoriesFromSqlite(filename,"object")
            except Exception as e:
                d.cancel()
                tkMessageBox.showerror("Database loading error", str(e))
                return
            d['value'] = 100
            self.window.title("Annotation Tool "+__version__+' : '+os.path.basename(self.params.videoFilename)+' : '+os.path.basename(filename))
            self.objectList.ReplaceList(data,  False)
            self.params.databaseFilename = filename
            self.ObjectModeLoader(check = False)
            self.readFrame(Next=False) 
            
    def SaveDatabase(self,event=None):
        self.playStatus=False
        filename=tkFileDialog.asksaveasfilename(initialdir = self.currendWorkingDirectory,title = "Saving the database",filetypes = [("SQLite","*.sqlite")])
        if len(filename)>0 :
            self.currendWorkingDirectory = os.path.dirname(filename)
            d = DialogBoxFactory.ProgressBarBox(self.window, title = 'Saving your data base', text = 'Loading current database features data..', determinateMode = 0, finishText = 'Saved at '+filename)
            d.go()
            for o in self.objectList.value:
                if o.getFeatures() is None and self.params.databaseFilename is not None and hasattr(o, 'featureNumbers'):
                    features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', o.featureNumbers)
                    o.setFeatures(features)
            #d['value']+=10
            if os.path.isfile(filename):
                d['label'] = 'Deleting the existing file..'
                os.remove(filename)
            try:
                #d['value']+=10
                d['label'] = 'Saving objects..'
                #length = len(self.objectList)
                #for i,o in enumerate(self.objectList):
                #    d['label'] = 'Saving object #{}'.format(o.getNum())
                #    print('Saving object #{}'.format(o.getNum()))
                storage.saveTrajectoriesToSqlite(outputFilename = filename, objects = self.objectList.value, trajectoryType='object')
                #    d['value'] = min(100, d['value']+80/length) if i!=length-1 else 100
                d.done()
                self.objectList.setSaved()
            except Exception as e:
                d.cancel()
                tkMessageBox.showerror("Saving the database", str(e))
                
    ###################################################DataBase management
                
    #Mode Management
    #Make the interface ready for the current value of self.mode
    #@featureManagementidx : List of features id(s) to load from database; ids are their positions in the staticListBox
    def ModeUpdateManager(self, event = None, featureManagementidx = [] ):
        self.playStatus=False
        self.ModeLabel['text']="You are in "+self.mode+' mode'
        self.Zoomable.setSelectionMode(False)
        self.staticListBox['state'] = 'normal'
        self.dynamicListBox['state'] = 'normal'
        self.slider['state'] = 'normal'
        self.previousButton['state'] = 'normal'
        if self.trackerBounding is not None:
                self.trackerBounding.deleteFromCanvas()
                self.trackerBounding = None
        for x in self.Points:
                x.deleteFromCanvas()
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            self.PrepareListBoxForObjectMode()
            self.Button2['text'] = "Merge selected objects"
            self.Button2['command'] = self.Fusion
            self.Button3['text'] = "Delete the selected objects"
            self.Button3['command'] = self.deleteObjects
            self.Button1['text']='Enable selection mode'
            if self.mode=='Object' : 
                self.HomeButton['state'] = 'disabled' 
        elif self.mode=='Feature':
            self.seeSelectedObjectsFeatures()
            self.PrepareListBoxForFeatureMode()
            self.Button2['text'] = "Make a local savepoint (Irreversible)"
            self.Button2['command'] = self.saveFeatures
            self.Button3['text'] = "Jump to the last local savepoint (Irreversible)"
            self.Button3['command'] = self.restoreFeatures
            self.Button1['text']='Enable selection mode'
            self.HomeButton['state'] = 'normal'
        elif self.mode=="feature management":
            self.PrepareListBoxForFeatureManagementMode(idx = featureManagementidx)
            self.Data = featureManagementidx
            self.Button2['text'] = "Create an objet with selected features"
            self.Button2['command'] = self.createANewObjectUsingFeatures
            self.Button3['text'] = "Delete selected features"
            self.Button3['command'] = self.deleteFeatures
            self.Button1['text']='Enable selection mode'
            self.HomeButton['state'] = 'normal'
        elif self.mode.startswith( 'Object creation' ):
            self.Button2['text'] = "Save"
            self.Button2['command'] = self.CreateObjectFromUserInput
            self.Button3['text'] = "Abort"
            self.Button3['command'] = self.ObjectModeLoader
            self.Button1['text']='Enable selection mode'
            self.PrepareListBoxForEditionMode()
            self.HomeButton['state'] = 'normal'
        elif self.mode=="Tracking":
            self.Zoomable.setInitTracker()
            self.previousButton['state'] = 'disabled'
            self.Button1['text'] = 'Delete and re-bound the tracker'
            self.Button2['text'] = "Save"
            self.Button2['command'] = self.CreateObjectUsingTracker
            self.Button3['text'] = "Upadate the tracker boundings"
            self.Button3['command'] = self.ReboundTracker
            self.PrepareListBoxForTrackerMode()
            self.HomeButton['state'] = 'normal'
        self.readFrame(Next=False, IgnoreCreation=True, InitializingTracker = True)
        
    #Sets loads the features of objects selected
    def seeSelectedObjectsFeatures(self):
        selected = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
        if len(selected)==0 and len(self.objectList.value)>0:
            selected = range(0,len(self.objectList.value))
        d = DialogBoxFactory.ProgressBarBox(self.window, title = 'Loading features', text = 'Loading current database features data..', determinateMode = 0, finishText = 'Done')
        d.go()
        for i,idx in enumerate(selected):
            d['label'] = 'Loading features for object #{}'.format(self.objectList[idx].getNum())
            if self.objectList[idx].features is None and self.params.databaseFilename is not None and hasattr(self.objectList[idx], 'featureNumbers'):
                try:
                    features = storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', self.objectList[idx].featureNumbers)
                except Exception as e:
                    d.cancel()
                    tkMessageBox.showerror("Features loading error", str(e))
                    return
                self.objectList[idx].setFeatures(features)
            d['value'] = 100*(i+1)/len(selected)
        self.Data = { idx : self.objectList[idx].features for idx in selected}
    
    #Switch to Object Mode
    def ObjectModeLoader(self, event=None, check=True):
        if check and self.saveStatus() and not tkMessageBox.askyesno("Save!","Do you want to ignore your last modifications ?"):
            return
        self.staticListBox['state'] = 'normal'
        self.dynamicListBox['state'] = 'normal'
        self.slider['state'] = 'normal'
        self.tracker = None
        if self.mode.startswith( 'Examination' ):
            self.mode='Object'
            self.SliderConfigurator()
            self.ModeLabel['text']="You are in "+self.mode+' mode'
        else :
            if not check and self.mode == 'feature management' and tkMessageBox.askyesno("Feature management mode","Do you want to stay on feature management mode ?"):
                for x in self.Points:
                    x.deleteFromCanvas()
                self.PrepareListBoxForObjectMode()
                self.PrepareListBoxForFeatureManagementMode(idx = self.Data)
                self.readFrame(Next=False)
                
            else :
                self.mode='Object'
                self.ModeUpdateManager()
        
    #Switch to feature mode
    def FeatureModeLoader(self):
        self.mode='Feature'
        self.ModeUpdateManager()
        
    #Switch to Examination of the object o where object o is the object which id is idx
    def setSliderObjectTimeInterval(self,idx):
        def tmp() :
            self.mode='Examination of the object #'+str(self.objectList[idx].getNum())
            self.ModeUpdateManager()
            self.VideoFrameManager.set_to_specific_frame(self.objectList[idx].getFirstInstant())
            self.SliderConfigurator(self.objectList[idx].getFirstInstant(),self.objectList[idx].getLastInstant())
            self.readFrame()
        return tmp
      
    #Switch to Edition mode [Point]    
    def EditionModeLoader(self):
        self.playStatus=False
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            self.mode = "Object creation [point]"
            self.ModeUpdateManager()
        else:
            tkMessageBox.showerror("Forbidden", 'Must go back to object mode first')
        
    #Switch to Edition mode [rectangle]
    def EditionModeLoader2(self):
        self.playStatus=False
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            self.mode = "Object creation [rectangle]"
            self.ModeUpdateManager()
        else:
            tkMessageBox.showerror("Forbidden", 'Must go back to object mode first')
        
    #Switch to tracker mode
    def TrackerModeLoader(self):
        self.playStatus=False
        if self.mode=='Object' or self.mode.startswith( 'Examination' ):
            self.mode = "Tracking"
            self.ModeUpdateManager()
        else:
            tkMessageBox.showerror("Forbidden", 'Must go back to object mode first')
        
    #Switch to Feature management mode for objects which ids are in idx list    
    def FeatureManagementModeLoader(self, idx = []):
        def tmp():
            self.mode = "feature management"
            self.ModeUpdateManager( featureManagementidx = idx)
        return tmp
    
    #Prepare listBoxes and Create the points for feature mode
    def PrepareListBoxForFeatureMode(self):
        self.staticListBox.delete(0, tk.END)
        self.Points=[]
        j=0
        for idx in self.Data:
            for i in self.Data[idx]:
                self.staticListBox.insert("end", str(i.getNum())+'\\'+str(self.objectList[idx].getNum()))
                ro=r()
                bl=r()
                gr=r()
                self.staticListBox.itemconfig(j, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
                self.staticListBox.itemconfig(j, {'fg':'#%02X%02X%02X' % (255-ro, 255-bl, 255-gr)})
                self.Points.append(CustomCanvasObjects.FeaturePoint(self.objectList[idx] , i , self, color='#%02X%02X%02X' % (ro,bl,gr),textcolor = '#%02X%02X%02X' % (255-ro,255-bl,255-gr) , idobj=str(i.getNum())+'\\'+str(idx), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix))
                j+=1
            
    #Prepare listBoxes and Create the points for feature management mode
    def PrepareListBoxForFeatureManagementMode(self, idx = []):
        self.staticListBox.delete(0, tk.END)
        self.Points=[]
        j=0
        if idx == [] :
            idx = self.objectList
        else:
            idx = [self.objectList[i] for i in idx]
        d = DialogBoxFactory.ProgressBarBox(self.window, title = 'Loading features', text = 'Loading current database features data..', determinateMode = 0, finishText = 'Done')
        d.go()
        for i,obj in enumerate(idx):
            d['label'] = 'Loading features for object #{}'.format(obj.getNum())
            if obj.getFeatures() is None and self.params.databaseFilename is not None and hasattr(obj, 'featureNumbers'):
                features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', obj.featureNumbers)
                obj.setFeatures(features)
            else:
                continue
            for f in obj.features:
                self.staticListBox.insert("end", str(f.getNum())+'\\'+str(obj.getNum()))
                ro=r()
                bl=r()
                gr=r()
                self.staticListBox.itemconfig(j, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
                self.staticListBox.itemconfig(j, {'fg':'#%02X%02X%02X' % (255-ro,255-bl,255-gr)})
                self.Points.append(CustomCanvasObjects.FeaturePoint(obj , f , self, color='#%02X%02X%02X' % (ro,bl,gr),textcolor = '#%02X%02X%02X' % (255-ro,255-bl,255-gr) , idobj=str(f.getNum())+'\\'+str(obj.getNum()), size=20,invHomography=self.invHomography, movable=False, newCamera = self.VideoFrameManager.newCameraMatrix))
                j+=1
            d['value'] = 100*(i+1)/len(idx)
    
    #Prepare listBoxes and Create the points for edition modes
    def PrepareListBoxForEditionMode(self):
        self.staticListBox.delete(0, tk.END)
        self.staticListBox.insert("end",'Edition Object')
        ro=r()
        bl=r()
        gr=r()
        self.staticListBox.itemconfig(0, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
        self.staticListBox.itemconfig(0, {'fg':'#%02X%02X%02X' % (255-ro,255-bl,255-gr)})
        if self.mode == "Object creation [rectangle]" :
            self.editionPointParams = dict(VideoTool = self, point_color = '#%02X%02X%02X' % (255-ro,255-bl,255-gr), color = '#%02X%02X%02X' % (ro,bl,gr) , size = 10, dashes = [3 , 2], invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix)
        else :
            self.editionPointParams = dict(VideoTool = self, color='#%02X%02X%02X' % (ro,bl,gr), textcolor = '#%02X%02X%02X' % (255-ro,255-bl,255-gr), idobj='E', invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix)
            
        tkMessageBox.showerror("Select the initial {}".format('rectangle' if self.mode == "Object creation [rectangle]" else 'point'), "Please {}".format('the initial rectangle region' if self.mode == "Object creation [rectangle]" else 'click to initialise the point'))
        self.Zoomable.setInitEditionPoint(self.mode == "Object creation [rectangle]")
        self.Points=[]
    
    #Prepare listBoxes and Create the points for tracker mode
    def PrepareListBoxForTrackerMode(self):
        self.staticListBox.delete(0, tk.END)
        self.staticListBox['state'] = 'disabled'
        self.dynamicListBox['state'] = 'disabled'
        self.slider['state'] = 'disabled'
        self.PushB1()
        
    #Prepare listBoxes and Create the points for object mode
    def PrepareListBoxForObjectMode(self):
        self.staticListBox.delete(0, tk.END)
        self.Points=[]
        for i in range(0, len(self.objectList.value)):
            self.staticListBox.insert("end", str(self.objectList[i].getNum()))
            if i >=  len(self.ObjectsColor):
                ro=r()
                bl=r()
                gr=r()
                self.staticListBox.itemconfig(i, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
                self.staticListBox.itemconfig(i, {'fg':'#%02X%02X%02X' % (255-ro,255-bl,255-gr)})
                self.ObjectsColor.append(('#%02X%02X%02X' % (ro,bl,gr) , '#%02X%02X%02X' % (255-ro,255-bl,255-gr)))
                self.Points.append(CustomCanvasObjects.FeaturePoint(self.objectList[i] , self.objectList[i] , self, color='#%02X%02X%02X' % (ro,bl,gr),textcolor = '#%02X%02X%02X' % (255-ro,255-bl,255-gr) , idobj=str(self.objectList[i].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False))
            else:
                self.staticListBox.itemconfig(i, {'bg': self.ObjectsColor[i][0] })
                self.staticListBox.itemconfig(i, {'fg': self.ObjectsColor[i][1]})
                self.Points.append(CustomCanvasObjects.FeaturePoint(self.objectList[i] , self.objectList[i] , self, color=self.ObjectsColor[i][0], textcolor = self.ObjectsColor[i][1] , idobj=str(self.objectList[i].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False))
            
        
    
    ##################################################Mode Management   
        
    
    #Utils
    
    #Creates necessary items to rebound the tracker
    def ReboundTracker(self):
        if self.trackerBounding is None :
            tkMessageBox.showerror("Updating boundings", "Please select tracking region")
        else :
            widthi, heighti = self.Zoomable.image.size
            widthi, heighti = int(self.Zoomable.imscale[0] * widthi), int(self.Zoomable.imscale[1] * heighti)
            p1, p2 = self.trackerBounding.getBoundings()
            p1 = (int(p1[0]/self.Zoomable.imscale[0]), int(p1[1]/self.Zoomable.imscale[1]) )
            p2 = (int(p2[0]/self.Zoomable.imscale[0]), int(p2[1]/self.Zoomable.imscale[1]) )
            self.tracker.correctBounding(p1 , p2)
    
    #Selects the points and items the lists
    #@ids : List of point.OvalItem of objects that must be selected
    #@keep : True if we must keep the last selection [When ctrl is pressed]
    def SelectPoints(self, ids, keep = False):
        self.staticListBox.unbind('<<ListboxSelect>>')
        self.dynamicListBox.unbind('<<ListboxSelect>>')
        fromstatic = self.Notebook.tab(self.Notebook.select(), "text") == 'Static list'
        if not keep:
            if fromstatic :
                self.staticListBox.selection_clear(0,self.staticListBox.size())
            else :
                self.dynamicListBox.selection_clear(0,self.dynamicListBox.size())
        for i,p in enumerate(self.Points):
            if not keep:
                if p.OvalItem in ids:
                    p.setSelect(True)
                else:
                    p.setSelect(False)
            else:
                if p.OvalItem in ids:
                    p.setSelect(not p.selected)
            if p.selected :
                if fromstatic:
                    self.staticListBox.selection_set(i)
                elif self.staticListBox.get(i) in self.dynamicListBox.get(0, tk.END):
                    self.dynamicListBox.selection_set(self.dynamicListBox.get(0, tk.END).index(self.staticListBox.get(i)))
        self.staticListBox.bind('<<ListboxSelect>>', self.ListSelection)
        self.dynamicListBox.bind('<<ListboxSelect>>', self.ListSelection)
    
    #Broadcast mouse motion signal to the other points
    def BroadcastMotion(self, event, from_obj):
        def tmp(p):
            if p != from_obj and p.selected and p.isReallyVisible() : 
                p.move(event,transmit=False)
        for x in self.Points:
                tmp(x)

    #Broadcast mouse relase signal to the other points
    def BroadcastRelease(self,event,from_obj):
        def tmp(p):
            if p != from_obj and p.selected and p.isReallyVisible(): 
                p.release(event,transmit=False)
        for x in self.Points:
                tmp(x)
    
    #returns a tuple which first value is the first availabe object number, and the second is the first availabe feature number
    def getAvailableObjectAndFeatureNumbers(self):
        objectNumbers = [-1]
        featureNumbers = [-1]
        for o in self.objectList.value:
            objectNumbers.append(o.getNum())
            if hasattr(o, 'featureNumbers'):
                featureNumbers += o.featureNumbers
        return max(objectNumbers)+1, max(featureNumbers)+1
    
    #returns an object background color value
    def getTheObjectColor(self,obj):
        return self.ObjectsColor[self.objectList.value.index(obj)][0]
            
    #make an object invisible
    #@ idx : items positions in the static listbox    
    def setAnObjectVisibility(self,idx, value):
        def tmp() :
            for i in idx:
                self.Points[i].setVisibility(value)
                self.staticListBox.delete(i)
                self.staticListBox.insert(i, str(self.Points[i].feature.getNum())+'\\'+str(self.Points[i].obj.getNum()) + (' (invisible)' if not self.Points[i].getVisibility() else '')+ ('[Non sauve]' if self.Points[i].canUndoPosition() else ''))
                self.staticListBox.itemconfig(i, {'bg':self.Points[i].getColors()[0]})
                self.staticListBox.itemconfig(i, {'fg':self.Points[i].getColors()[1]})
                self.readFrame(Next = False)
            
        return tmp
    
    
    #show if an item is unsaved and/or invisible
    def showVisibilitiAndSaveStatutForListItems(self):
        for idx,p in enumerate(self.Points):
            self.staticListBox.delete(idx)
            self.staticListBox.insert(idx, str(p.feature.getNum())+'\\'+str(p.obj.getNum()) + (' (invisible)' if not p.getVisibility() else '')+ ('[Unsaved]' if p.canUndoPosition() else ''))
            self.staticListBox.itemconfig(idx, {'bg':p.getColors()[0]})
            self.staticListBox.itemconfig(idx, {'fg':p.getColors()[1]})
        self.readFrame(Next = False)
    
    #returns true if something must be saved
    def saveStatus(self, Points=None):
        if self.mode in ['Feature', "Object creation", "feature management"]:
            if Points is None:
                Points = self.Points
            for i in Points:
                if i.canUndoPosition():
                    return True
            return False
        else:
            if self.mode == 'Tracking':
                return True
            return not self.objectList.saved
            
    ########################################Utils
            
    

    

      
      
    #Object Management
      
    #Allows to change the type of the object which idx is its position in the staticListBox
    def SetObjectType(self, idx):
        def tmp():
            d =  DialogBoxFactory.ChoiceDialog(self.window,
                 text='Choose your object type', choices=moving.userTypeNames, 
                 title='Choose',CancelCode='unknown')
            res = d.go()
            self.objectList[idx].userType = moving.userType2Num[res]
            self.readFrame(Next = False)
        return tmp
        
    
    #Allows to change the number of object of the object which idx is its position in the staticListBox
    def SetObjectNObjects(self, idx):
        def tmp():
            self.objectList[idx].setNObjects(tkSimpleDialog.askinteger('Set the number of object tracked','Enter an integer', minvalue = 1, initialvalue = self.objectList[idx].nObjects if self.objectList[idx].nObjects is not None else 1, parent = self.window))
        return tmp
        
        
    #Allows to change the time interval of the object which idx is its position in the staticListBox
    #Begin is True if the user wants to change the start interval
    def CropObjectTimeInterval(self, idx, begin = True):
        def tmp():
            timeInt = self.objectList[idx].getTimeInterval()
            minval = timeInt.first if begin else timeInt.first + self.params.minFeatureTime
            maxval = timeInt.last - self.params.minFeatureTime if begin else timeInt.last
            if maxval < minval :
                tkMessageBox.showerror("Cannot change this object time interval", 'min-feature-time constaint will be violated ! ')
                return
            value = tkSimpleDialog.askinteger('Set {} value of '.format('startFrame' if begin else 'endFrame'), 'Enter an integer between {} and {}'.format(minval, maxval), minvalue = minval, initialvalue = minval, maxvalue = maxval, parent = self.window)
            if value is not None:
                self.objectList.cropObject(idx, value = value, after = not begin)
                self.Points[idx].deleteFromCanvas()
                self.Points[idx] = CustomCanvasObjects.FeaturePoint(self.objectList[idx] , self.objectList[idx] , self, color=self.ObjectsColor[idx][0], textcolor = self.ObjectsColor[idx][1] , idobj=str(self.objectList[idx].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False)
                self.Points[idx].setToFrame(self.frameNumber-1)
        return tmp
        
    #Allows to crop the time interval of the object which idx is its position in the staticListBox to the current time interval
    def CropObjectToNow(self, idx):
        def tmp():
            if self.objectList[idx].features is None and self.params.databaseFilename is not None and hasattr(self.objectList[idx], 'featureNumbers'):
                features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', self.objectList[idx].featureNumbers)
                self.objectList[idx].setFeatures(features)
            self.objectList.cropObject(idx, value = self.frameNumber-1, after = True)
            self.Points[idx].deleteFromCanvas()
            self.Points[idx] = CustomCanvasObjects.FeaturePoint(self.objectList[idx] , self.objectList[idx] , self, color=self.ObjectsColor[idx][0], textcolor = self.ObjectsColor[idx][1] , idobj=str(self.objectList[idx].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False)
            self.Points[idx].setToFrame(self.frameNumber-1)
        return tmp
        
    #Returns True if the user can crop the time interval of the object which idx is its position in the staticListBox to the current time interval
    #Note : It takes in account the fact that an object have a length >= minFeatureTime
    def CanCropToNow(self, idx):
        timeInt = self.objectList[idx].getTimeInterval()
        return (self.frameNumber-1) - timeInt.first >= self.params.minFeatureTime
        
       
    #Allows to split object which idx is its position in the staticListBox into 2 objects
    def splitObjectUI(self, idx):
        def tmp():
            timeInt = self.objectList[idx].getTimeInterval()
            minval = timeInt.first + self.params.minFeatureTime
            maxval = timeInt.last - self.params.minFeatureTime
            if maxval < minval :
                tkMessageBox.showerror("Cannot change this object time interval", 'min-feature-time constaint will be violated ! ')
                return
            value = tkSimpleDialog.askinteger('Set value for object #{} splitting'.format(self.objectList[idx].getNum()), 'Enter an integer between {} and {}'.format(minval, maxval), minvalue = minval, initialvalue = minval, maxvalue = maxval, parent = self.window)
            if value is not None:
                ro=r()
                bl=r()
                gr=r()
                oid, fid = self.getAvailableObjectAndFeatureNumbers()
                if self.objectList[idx].features is None and self.params.databaseFilename is not None and hasattr(self.objectList[idx], 'featureNumbers'):
                    features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', self.objectList[idx].featureNumbers)
                    self.objectList[idx].setFeatures(features)
                self.ObjectsColor.insert(idx+1 ,('#%02X%02X%02X' % (ro,bl,gr) , '#%02X%02X%02X' % (255-ro,255-bl,255-gr)))
                self.objectList.splitObject(idx, value = value, objId = oid, FeatureNumOffset = fid)
                self.staticListBox.insert(idx+1, str(self.objectList[idx+1].getNum()))
                self.staticListBox.itemconfig(idx+1, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
                self.staticListBox.itemconfig(idx+1, {'fg':'#%02X%02X%02X' % (255-ro,255-bl,255-gr)})
                self.Points[idx].deleteFromCanvas()
                self.Points[idx] = CustomCanvasObjects.FeaturePoint(self.objectList[idx] , self.objectList[idx] , self, color=self.ObjectsColor[idx][0], textcolor = self.ObjectsColor[idx][1] , idobj=str(self.objectList[idx].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False)
                self.Points.insert(idx+1, CustomCanvasObjects.FeaturePoint(self.objectList[idx+1] , self.objectList[idx+1] , self, color=self.ObjectsColor[idx+1][0], textcolor = self.ObjectsColor[idx+1][1] , idobj=str(self.objectList[idx+1].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False))
                self.Points[idx].setToFrame(self.frameNumber-1)
                self.Points[idx+1].setToFrame(self.frameNumber-1)
        return tmp
        
    #Allows to split the time interval of the object which idx is its position in the staticListBox. The first object would last from original object startFrameNumber to now and the second from now to the endFrameNumber
    def splitObjectAtNow(self, idx):
        def tmp():
            ro=r()
            bl=r()
            gr=r()
            oid, fid = self.getAvailableObjectAndFeatureNumbers()
            if self.objectList[idx].features is None and self.params.databaseFilename is not None and hasattr(self.objectList[idx], 'featureNumbers'):
                features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', self.objectList[idx].featureNumbers)
                self.objectList[idx].setFeatures(features)
            self.ObjectsColor.insert(idx+1 ,('#%02X%02X%02X' % (ro,bl,gr) , '#%02X%02X%02X' % (255-ro,255-bl,255-gr)))
            self.objectList.splitObject(idx, value = self.frameNumber-1, objId = oid, FeatureNumOffset = fid)
            self.staticListBox.insert(idx+1, str(self.objectList[idx+1].getNum()))
            self.staticListBox.itemconfig(idx+1, {'bg':'#%02X%02X%02X' % (ro,bl,gr)})
            self.staticListBox.itemconfig(idx+1, {'fg':'#%02X%02X%02X' % (255-ro,255-bl,255-gr)})
            self.Points[idx].deleteFromCanvas()
            self.Points[idx] = CustomCanvasObjects.FeaturePoint(self.objectList[idx] , self.objectList[idx] , self, color=self.ObjectsColor[idx][0], textcolor = self.ObjectsColor[idx][1] , idobj=str(self.objectList[idx].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False)
            self.Points.insert(idx+1, CustomCanvasObjects.FeaturePoint(self.objectList[idx+1] , self.objectList[idx+1] , self, color=self.ObjectsColor[idx+1][0], textcolor = self.ObjectsColor[idx+1][1] , idobj=str(self.objectList[idx+1].getNum()), size=20,invHomography=self.invHomography, newCamera = self.VideoFrameManager.newCameraMatrix, movable = False))
            self.Points[idx].setToFrame(self.frameNumber-1)
            self.Points[idx+1].setToFrame(self.frameNumber-1)
        return tmp
        
    #Returns True if the user can call splitObjectAtNow
    #Note : It takes in account the fact that an object have a length >= minFeatureTime
    def CanSplitAtNow(self, idx):
        timeInt = self.objectList[idx].getTimeInterval()
        return (self.frameNumber-1) - timeInt.first >= self.params.minFeatureTime and timeInt.last - (self.frameNumber-1) >= self.params.minFeatureTime
    
      
    #Allows to delete the selected features from their objects
    def deleteFeatures(self):
        idxs = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
        if len(idxs)==0:
            tkMessageBox.showerror("Deleting features", "Select at least 1 feature")
        else:
            Features = [(p.feature,p.obj) for i,p in enumerate(self.Points) if i in idxs ]
            _, FeatureId = self.getAvailableObjectAndFeatureNumbers()
            try :
                self.objectList.delFeatures(Features, FeatureId)
            except Exception as e:
                tkMessageBox.showerror("Configuration file error", str(e))
                return
                
                
        self.ObjectModeLoader(check = False)
    
    #Allows to delete the selected features from the list 
    def deleteObjects(self):
        self.playStatus=False
        idxList = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
        if len(idxList)==0 :
            tkMessageBox.showerror("Deleting Objects", "Choose at least an object")
        else:
            self.objectList.delObjects(idxList)
            for i in idxList:
                self.staticListBox.delete(i)
            self.ModeUpdateManager()
    
    #Creates an object from data comming from points or rectangle in Object creation mode    
    def CreateObjectFromUserInput(self):
        t, p, v = self.Points[0].getTimeIntervalPositionsAndVelocities()
        d =  DialogBoxFactory.ChoiceDialog(self.window,
                 text='Le type du vehicule', choices=moving.userTypeNames, 
                 title='Choose',CancelCode='unknown')
        res = d.go()
        ObjId, FeatureId = self.getAvailableObjectAndFeatureNumbers()
        obj = moving.MovingObject(ObjId , t, p, v, userType = moving.userType2Num[res])
        obj.features = [copy.deepcopy(obj)]
        obj.features[0].num = FeatureId
        obj.featureNumbers = [FeatureId]
        self.objectList+=[obj]
        self.ObjectModeLoader(check = False)
        
    #Creates an object from data comming from the tracker    
    def CreateObjectUsingTracker(self):
        self.playStatus=False
        if self.tracker is None :
            tkMessageBox.showerror("Creating an object using tracker results", "Initialize the tracker first")
        else :
            d =  DialogBoxFactory.ChoiceDialog(self.window,
                 text='Choose your object type', choices=moving.userTypeNames, 
                 title='Choose',CancelCode='unknown')
            typeOfObject = d.go()
            objId, FeatureId = self.getAvailableObjectAndFeatureNumbers()
            add = self.tracker.generateObject(homography=self.params.homography, newCameraMatrix=self.VideoFrameManager.newCameraMatrix, offsetFeatureNumber = FeatureId, objectNum = objId, userType = moving.userType2Num[typeOfObject])
            if add is None:
                tkMessageBox.showerror("Creating an object using tracker results", "Not enough results to build an object")
            else :
                self.objectList+=[add]
            self.ObjectModeLoader(check = False)
    
    #Merges 2 selected objects        
    def Fusion(self):
        self.playStatus=False
        idx = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
        if len(idx)!=2 :
            tkMessageBox.showerror("Fusion", "Select exactly 2 objects")
        else:
            self.seeSelectedObjectsFeatures()
            _, featureId = self.getAvailableObjectAndFeatureNumbers()
            if  self.params.databaseFilename is not None and hasattr(self.objectList[idx[0]], 'featureNumbers'): # load features to save the new feature (if generated)
                for i in idx:
                    if not self.objectList[i].hasFeatures():
                        features=storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, 'feature', self.objectList[i].featureNumbers)
                        self.objectList[i].setFeatures(features)
            self.objectList.concatenateWith(idx[0],idx[1], featureId)
            self.ModeUpdateManager()
    
    #Creates an object from data comming from the selected features        
    def createANewObjectUsingFeatures(self):
        idxs = list(self.staticListBox.curselection() if self.Notebook.tab(self.Notebook.select(), "text") == 'Static list' else [self.staticListBox.get(0, tk.END).index(self.dynamicListBox.get(i)) for i in self.dynamicListBox.curselection()])
        if len(idxs)==0:
            tkMessageBox.showerror("Creating an object by features", "Select at least 1 feature")
        else:
            d =  DialogBoxFactory.ChoiceDialog(self.window,
                 text='Choose your object type', choices=moving.userTypeNames, 
                 title='Choose',CancelCode='unknown')
            res = d.go()
            objId, FeatureId = self.getAvailableObjectAndFeatureNumbers()
            Features = [p.feature for i,p in enumerate(self.Points) if i in idxs ]
            n = self.objectList.createObjectByFeatures( Features, objId, moving.userType2Num[res], FeatureId)
            if n>1:
                tkMessageBox.showinfo("Information", 'There were gaps between objects intervals : {} objects have been created'.format(n), parent = self.window)
        self.ObjectModeLoader(check = False)
    
    ##############################Object Management
                
           

    #Video Management  
    
    def onNextFrameButtonDoubleClick(self, event):
        if self.mode!='Tracking':
            self.VideoFrameManager.set_to_specific_frame(self.frameNumber+self.frameJump)
        self.readFrame()
    
    #Reads the next frame
    #@hasSourceChanged : True if a new video has just been loaded
    #@setOnPlay : True if auto-play must be continued
    #@Next : False if must reload the current frame 
    #@IgnoreCreation : True if on Object creation mode if we must load one frame on self.frameJump 
    #@InitializingTracker : True if tracker has just been initialized
    def readFrame(self, hasSourceChanged=False, setOnPlay=False, Next=True, IgnoreCreation=False, InitializingTracker = False):
        if self.mode.startswith( 'Object creation' ) and not IgnoreCreation:
            if self.Points == []:
                self.playStatus=False
                tkMessageBox.showerror("Select the initial {}".format('rectangle' if self.mode == "Object creation [rectangle]" else 'point'), "Please {}".format('the initial rectangle region' if self.mode == "Object creation [rectangle]" else 'click to initialise the point'))
                self.Zoomable.setInitEditionPoint(self.mode == "Object creation [rectangle]")
        if self.mode=='Feature' or self.mode.startswith( 'Object creation' ) or self.mode=="feature management":
            ret, frame = self.VideoFrameManager.get_frame(Next)
        elif self.mode=='Object' or self.mode.startswith( 'Examination' ) :
            ret, frame = self.VideoFrameManager.getConfiguredFrame(Next)
        elif self.mode=='Tracking':
            if self.trackerBounding is None and not InitializingTracker:
                self.playStatus=False
                tkMessageBox.showerror("Updating boundings", "Please select tracking region")
                self.Zoomable.setInitTracker()
                ret = False
            elif InitializingTracker :
                ret, frame = self.VideoFrameManager.get_frame(Next)
            else:
                ret, frame = self.VideoFrameManager.get_frame(Next, self.tracker)
                if not self.tracker.onTracking :
                    if tkMessageBox.askyesno("Tracker error", "The tracker lost your object. Do you want to save ?"):
                        self.CreateObjectUsingTracker()
                        return
                    else :
                        self.ObjectModeLoader(check = False)
                        return
                p1, p2 = self.tracker.getBounding()
                if p1==(None, None) or p2==(None, None) :
                    self.trackerBounding.deleteFromCanvas()
                    self.trackerBounding = None
                else :
                    p1 = (int(p1[0]*self.Zoomable.imscale[0]), int(p1[1]*self.Zoomable.imscale[1]) )
                    p2 = (int(p2[0]*self.Zoomable.imscale[0]), int(p2[1]*self.Zoomable.imscale[1]) )
                    self.trackerBounding.setBoundings(p1, p2)
        if ret:
            self.frameNumber = self.VideoFrameManager.get_current_frame_pos()
            self.playStatus=False
            # removed for MacOS: useful for Linux?
            # if self.Zoomable is not None and self.Zoomable.imsize is not None and not self.deform:
            #     if self.lastDimensions is None or self.lastDimensions != self.Zoomable.imsize:
            #         self.lastDimensions = self.Zoomable.imsize[0], self.Zoomable.imsize[1]
            #         w, h = self.Zoomable.imsize
            #         self.PanedWindow.paneconfigure(self.videoTopLabeledFrame, width = w)
            #         self.CentralPanedWindow.paneconfigure(self.ZoomableMasterFrame, height = h)    
                    
            if self.slider['to'] < self.frameNumber-1 and self.slider['to']!=-1 :
                if tkMessageBox.askyesno('Warning','You will go back to object mode. Do you want to continue ?') :
                    self.ObjectModeLoader(check = False)
                else:
                    return
            if hasSourceChanged:
                self.Zoomable.change_format(Image.fromarray(frame))
                
            else:
                self.Zoomable.set_image(image = Image.fromarray(frame))
            self.slider.set(self.frameNumber-1)
            self.playStatus=setOnPlay
            for x in self.Points:
                x.setToFrame(self.frameNumber-1)
            j = 0
            self.dynamicListBox.delete(0,tk.END)
            for i in range(0,self.staticListBox.size()) :
                if (self.mode=='Object' or self.mode.startswith( 'Examination' )) and self.objectList[i].existsAtInstant(self.frameNumber-1):
                    self.dynamicListBox.insert(tk.END , str(self.objectList[i].getNum()))
                    self.dynamicListBox.itemconfig(j, {'bg': self.ObjectsColor[i][0], 'fg' : self.ObjectsColor[i][1]})
                    j+=1
                elif (self.mode=='Feature' or self.mode=="feature management") and self.Points[i].isReallyVisible():
                    self.dynamicListBox.insert(tk.END , self.Points[i].idtext)
                    self.dynamicListBox.itemconfig(j, {'bg': self.Points[i].color, 'fg' : self.Points[i].textcolor})
                    j+=1
                elif self.mode.startswith( 'Object creation' ):
                    self.dynamicListBox.insert(tk.END , 'Edition Object')
                    self.dynamicListBox.itemconfig(j, {'bg': self.editionPointParams['color'], 'fg' : self.editionPointParams['point_color'] if self.mode == "Object creation [rectangle]" else self.editionPointParams['textcolor'] })
                    j+=1
    
    #reads the previous frame
    def readPreviousFrame(self):
        try:
            self.playStatus=False
            if self.slider['from'] > self.frameNumber-2 and self.slider['from'] != -1 :
                if tkMessageBox.askyesno('Warning','You will go back to object mode. Do you want to continue ?') :
                    self.SliderConfigurator()
                    self.ObjectModeLoader()
                else:
                    return
            self.VideoFrameManager.set_to_specific_frame(self.frameNumber-1)
            self.frameNumber -= 1
            self.readFrame(IgnoreCreation = True)
        except :
            pass
            
    ###############################Video Management
    
   
    #Reading Files
    def readConfigurationFile(self, filename, databaseFilename = None, videoFilename = None, homographyFilename = None):
        try:
            d = DialogBoxFactory.ProgressBarBox(self.window, title = 'Loading config file', text = 'Reading the file...', determinateMode = 0, finishText = 'Done')
            d.go()
            self.params = storage.ProcessParameters(filename)
            if homographyFilename is not None:
                self.params.homography = numpy.loadtxt(homographyFilename)
            d['value'] = 10
        except Exception as e:
            d.cancel()
            tkMessageBox.showerror("Configuration file error", str(e))
            return
        d['label'] = 'Computing homography'
        if self.params.homography is not None:
            self.invHomography = numpy.linalg.inv(self.params.homography)
        else:
            self.invHomography = None
        intrinsicCameraMatrix = self.params.intrinsicCameraMatrix
        distortionCoefficients = numpy.array(self.params.distortionCoefficients)
        undistortedImageMultiplication = self.params.undistortedImageMultiplication
        undistort = self.params.undistort
        d['value'] = 20
        if databaseFilename is not None:
            self.params.databaseFilename = databaseFilename
        if self.params.databaseFilename is not None:
            d['label'] = 'Loading the new data base'
            self.objectList.ReplaceList(storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, "object"), True)
            d['value'] = 50
        boundingBoxes = storage.loadBoundingBoxTableForDisplay(self.params.databaseFilename) if self.params.databaseFilename is not None else {}
        d['label'] = 'Setting the new video'
        if videoFilename is not None:
            self.params.videoFilename = videoFilename
        self.VideoFrameManager= VideoFrameManager.VideoFrameManager(self, self.params.videoFilename, boundingBoxes, undistort, intrinsicCameraMatrix = intrinsicCameraMatrix, distortionCoefficients = distortionCoefficients, undistortedImageMultiplication = undistortedImageMultiplication, colorBlind = False)
        d['value'] = 100
        self.mode='Object'
        self.ModeUpdateManager()
        self.readFrame(hasSourceChanged=True)
        title = "Annotation Tool "+__version__+' : '+os.path.basename(self.params.videoFilename)
        if self.params.databaseFilename is not None:
            title += ' : '+os.path.basename(self.params.databaseFilename)
        self.window.title(title)
        if not self.deform :
                w, h = self.Zoomable.imsize
                self.window.geometry('{}x{}+0+0'.format(w + deltax, h + deltay))
        self.SliderConfigurator()
            
    def LoadConfigurationFile(self, event = None, databaseFilename = None, homographyFilename = None):
        self.playStatus=False
        if self.saveStatus() and not tkMessageBox.askyesno("Save!","Do you want to ignore your last modifications ?"):
            return
        filename=tkFileDialog.askopenfilename(initialdir = self.currendWorkingDirectory,title = "Choose your configuration file",filetypes = [("configuration file","*.ini"),("configuration file","*.cfg")])
        if len(filename)>0 :
            self.currendWorkingDirectory = os.path.dirname(filename)
            self.readConfigurationFile(filename, databaseFilename = databaseFilename, homographyFilename = homographyFilename)
            
            
    def LoadVideo(self, event = None, databaseFilename = None, homographyFilename = None):
        self.playStatus=False
        if self.saveStatus() and not tkMessageBox.askyesno("Save!","Do you want to ignore your last modifications ?"):
            return
        videoFilename=tkFileDialog.askopenfilename(initialdir = self.currendWorkingDirectory,title = "Choose your video file",filetypes = [("AVI", "*.avi"), ("WebM","*.webm"), ("Matroska", "*.mkv"), ("Flash Video (FLV)", "*.flv"), ("Flash Video (FLV)", "*.f4v"), ("Flash Video (FLV)", "*.f4p"),("Flash Video (FLV)", "*.f4a"), ("Flash Video (FLV)", "*.f4b"), ("Ogg Video", "*.ogg"), ("Ogg Video", "*.ogv"), ("QuickTime File Format", "*.mov"), ("QuickTime File Format", "*.qt"), ("Windows Media Video", "*.wmv"), ("MPEG-4 Part 14 (MP4)", "*.mp4"), ("MPEG-4 Part 14 (MP4)", "*.m4p"), ("MPEG-4 Part 14 (MP4)", "*.m4v"), ("MPEG", "*.mpg"), ("MPEG", "*.mp2"), ("MPEG", "*.mpeg"), ("MPEG", "*.mpe"), ("MPEG", "*.mpv"), ("3GPP", "*.3gp"), ("3GPP", "*.3g2"), ("All files", "*.*"), ])
        if len(videoFilename)>0 :
            self.currendWorkingDirectory = os.path.dirname(videoFilename)
            self.readVideoFile(videoFilename, databaseFilename, homographyFilename)
            self.objectList.setSaved()

    def readVideoFile(self, filename, databaseFilename = None, homographyFilename = None):
        if not hasattr(self, 'params'):
            class MockParameters():
                pass
            self.params = MockParameters()
            self.params.videoFilename = filename
            self.params.databaseFilename = databaseFilename
            self.params.homography = homographyFilename
            self.params.minFeatureTime = 10
        else:
            self.params.videoFilename = filename
            self.params.databaseFilename = databaseFilename
            self.params.homographyFilename = homographyFilename
        if homographyFilename is not None:
            self.params.homography = numpy.loadtxt(homographyFilename)
        else:
            self.params.homography = None
        if self.params.homography is not None:
            self.invHomography = numpy.linalg.inv(self.params.homography)
        else:
            self.invHomography = None
        try:
            self.VideoFrameManager = VideoFrameManager.VideoFrameManager(self, self.params.videoFilename)
        except Exception as e:
            tkMessageBox.showerror("Video file loading", str(e))
            return
        if self.params.databaseFilename is not None :
            self.objectList.ReplaceList(storage.loadTrajectoriesFromSqlite(self.params.databaseFilename, "object"), False)
        else :
            self.objectList.ReplaceList([], False)
        filename = os.path.basename(filename)
        self.window.title("Annotation Tool "+__version__+" : "+filename)
        self.ObjectModeLoader(check = False)
        self.readFrame(hasSourceChanged=True)
        if not self.deform :
            try:
                w, h = self.Zoomable.imsize
                self.window.geometry('{}x{}+0+0'.format(w + deltax, h + deltay))
            except:
                tkMessageBox.showerror("Video source error", "Your file is '{}' corrupted ".format(filename))
                delattr(self.params,'videoFilename')
        self.SliderConfigurator()
        
    
    
    ###############################Reading Files




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='The program aims to annotate trafficintelligence data.')
    parser.add_argument('--cfg', dest = 'configFilename', help = 'path to the configuration file')
    parser.add_argument('-d', dest = 'databaseFilename', help = 'path to the database file')
    parser.add_argument('-i', dest = 'videoFilename', help = 'path to the video file')
    parser.add_argument('-o', dest = 'homographyFilename', help = 'path to the homography file')
    parser.add_argument('--width', dest = 'width', help = 'width for the video', type = int)
    parser.add_argument('--height', dest = 'height', help = 'height for the video', type = int)
    parser.add_argument('--undistort', dest = 'deform', help = 'deform the video or not', type = bool, default = False)
    parser.add_argument('--thickness', dest = 'thickness', help = 'thickness for drawing items', type = int, default = 5)
    parser.add_argument('-s', dest = 'frameJump', help = 'Jump length between frames', type = int, default = 4)
    args = parser.parse_args()
    window = tk.Tk()
    if args.configFilename is not None:
        args.configFilename = os.path.abspath(args.configFilename)
    if args.databaseFilename is not None:
        args.databaseFilename = os.path.abspath(args.databaseFilename)
    if args.videoFilename is not None:
        args.videoFilename = os.path.abspath(args.videoFilename)
    if args.homographyFilename is not None:
        args.homographyFilename = os.path.abspath(args.homographyFilename)
    if args.width is None:
        args.width = window.winfo_screenwidth() -deltax -100
    if args.height is None:
        args.height = window.winfo_screenheight() -deltay -100
    VideoTool(window, "Annotation Tool "+__version__, configFilename = args.configFilename, width = args.width, height = args.height, thickness = args.thickness, deform = args.deform, databaseFilename = args.databaseFilename, videoFilename = args.videoFilename, homographyFilename = args.homographyFilename, frameJump = args.frameJump )
