from __future__ import division
import tkinter as tk
from tkinter import ttk 
import math
import CustomCanvasItemSelector
import CustomCanvasObjects
import Tracker
import sys, os, os.path
from PIL import ImageTk, Image

class ZoomableFrame(ttk.Frame):
    ''' Simple zoom with mouse wheel '''
    def __init__(self, data, mainframe, width=600, height = 800):
        ''' Initialize the main Frame '''
        ttk.Frame.__init__(self, master = mainframe)
        vbar = tk.Scrollbar(self.master, orient='vertical')
        hbar = tk.Scrollbar(self.master, orient='horizontal')
        vbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        hbar.pack(fill=tk.X, side=tk.BOTTOM, expand=tk.FALSE)
        self.image= None
        self.size = (width, height)
        self.canvas = tk.Canvas(self.master, highlightthickness=0,
                                xscrollcommand=hbar.set, yscrollcommand=vbar.set, width = width, height = height)
        self.canvas.pack(fill=tk.BOTH, expand = 1)
        vbar.configure(command=self.canvas.yview)  # bind scrollbars to the canvas
        hbar.configure(command=self.canvas.xview)
        #self.master.rowconfigure(0, weight=1)
        #self.master.columnconfigure(0, weight=1)
        self.data = data
        self.canvas.bind('<MouseWheel>', self.wheel)  # with Windows and MacOS, but not Linux
        self.canvas.bind('<Button-5>',   self.wheel)  # only with Linux, wheel scroll down
        self.canvas.bind('<Button-4>',   self.wheel)  # only with Linux, wheel scroll up
        self.canvas.tag_bind('current', '<Button-3>', self.canvasB3Use)
        self.canvas.bind('<Configure>',  self.change_size)
        #self.pack(fill=tk.BOTH, expand = 1)
        self.imscale = (1.0 , 1.0)
        self.imageid = None
        self.delta = 0.5 #Zooming incremental
        self.minScale= (1.0, 1.0)
        self.imsize = None
        self.rect = CustomCanvasItemSelector.Selector(self.canvas, data)
        self.setSelectionMode(False)
        
        #For auto scrolling
        self.xlastview=[] 
        self.ylastview=[]
        self.selectedLast = []
        
        minsize, maxsize = 5, 20
        
        if self.image is not None:
            self.show_image()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))
    
    def canvasB3Use(self, event):
        self.data.SelectPoints([])
        self.data.B2MouseMenuContent(event)
    
    
    def setSelectionMode(self, mode):
        def onDrag(start, end, keep=False, stop=True):
            items = self.rect.hit_test(start, end)
            ids=[]
            for x in self.rect.items:
                if x in items:
                    ids.append(x)
            if len(ids)<len(self.selectedLast) :
                modif = [x for x in self.selectedLast if x not in ids]
                self.selectedLast = ids
                self.data.SelectPoints(modif,keep)
            else :
                ids=[x for x in ids if x not in self.selectedLast ]
                self.selectedLast+=ids
                self.data.SelectPoints(ids,keep)
            if stop:
                self.selectedLast = []
            
        if mode:
            self.rect.autodraw(fill="", width=2, command=onDrag)
        else:
            self.rect.stopAutodraw()
            
    def setInitTracker(self):
        self.rect.stopAutodraw()
        def InitTracker(start, end):
            widthi, heighti = self.image.size
            widthi, heighti = int(self.imscale[0] * widthi), int(self.imscale[1] * heighti)
            end = (min(max(end[0],0),widthi), min(max(end[1],0),heighti))
            start , end = ( int(min(start[0],end[0])/self.data.Zoomable.imscale[0]) , int(min(start[1],end[1])/self.data.Zoomable.imscale[1])  ) , ( int(max(start[0],end[0])/self.data.Zoomable.imscale[0]) , int(max(start[1],end[1])/self.data.Zoomable.imscale[1])  )
            self.rect.stopAutodraw()
            if self.data.tracker is None :
                self.data.tracker = Tracker.Tracker(self.data.VideoFrameManager.get_frame(Next = False)[1], self.data.frameNumber-1, startPoint = start, endPoint= end, ** dict(self.data.params.__dict__) )
            else :
                self.data.tracker.correctBounding(start, end)
            self.data.trackerBounding = CustomCanvasObjects.EditableRectangle(VideoTool = self.data, point_color = 'black', color = 'blue' , size = 10,  p1=(int(start[0]*self.data.Zoomable.imscale[0]), int(start[1]*self.data.Zoomable.imscale[1])), p2=(int(end[0]*self.data.Zoomable.imscale[0]), int(end[1]*self.data.Zoomable.imscale[1])), dashes = [3 , 2] )
        self.rect.autodraw(fill="", width=2, command=InitTracker, autoUpdate=False)
        
    def setInitEditionPoint(self, rectangle = False):
        self.rect.stopAutodraw()
        def initCoords(start, end):
            widthi, heighti = self.image.size
            widthi, heighti = int(self.imscale[0] * widthi), int(self.imscale[1] * heighti)
            end = (min(max(end[0],0),widthi), min(max(end[1],0),heighti))
            start , end = ( int(min(start[0],end[0])/self.data.Zoomable.imscale[0]) , int(min(start[1],end[1])/self.data.Zoomable.imscale[1])  ) , ( int(max(start[0],end[0])/self.data.Zoomable.imscale[0]) , int(max(start[1],end[1])/self.data.Zoomable.imscale[1])  )
            self.rect.stopAutodraw()
            if not rectangle:
                self.data.Points= [CustomCanvasObjects.EditablePointWithTimeManagement(xpos = int((start[0]+end[0] )/2), ypos = int((start[1]+end[1] )/2), **self.data.editionPointParams)]
            else :
                self.data.Points= [CustomCanvasObjects.EditableRectangleWithTimeManagement(p1 = start, p2 = end, **self.data.editionPointParams)]
            self.data.Points[0].setToFrame(self.data.frameNumber-1)
        self.rect.autodraw(fill="", width=2, command=initCoords, autoUpdate=False)
            
            
    
    def change_format(self, image):
        width, height = image.size
        if self.data.deform:
            zoomingx, zoomingy = self.size[0] / width, self.size[1] / height
        else:
            zoomingx = min(self.size[0] / width ,self.size[1] / height)
            zoomingy = zoomingx
        new_size = int(math.ceil(zoomingx * width)), int(math.ceil(zoomingy * height))
        self.image=image
        if self.imageid:
            self.canvas.delete(self.imageid)
            self.imageid = None
            self.canvas.imagetk = None
        imagetk = ImageTk.PhotoImage(self.image.resize(new_size))
        im=self.image.resize(new_size)
        self.imsize = int(math.ceil(zoomingx * width)), int(math.ceil(zoomingy * height))
        self.imageid = self.canvas.create_image(0,0,
                                                anchor='nw', image=imagetk)
        self.canvas.lower(self.imageid)  
        self.canvas.imagetk = imagetk  # keep an extra reference to prevent garbage-collection
        self.imscale=(zoomingx, zoomingy)
        self.minScale=(zoomingx, zoomingy)
        self.canvas.scale('all', 0 , 0 ,zoomingx,zoomingy)
        self.canvas.configure(scrollregion=(0, 0, int(math.ceil(zoomingx * width)), int(math.ceil(zoomingy * height))))
        self.canvas.config(width = int(math.ceil(zoomingx * width)), height = int(math.ceil(zoomingy * height)) )
        #self.config(width = int(math.ceil(zoomingx * width)), height = int(math.ceil(zoomingy * height)) )
        #self.data.window.geometry('{}x{}'.format(900,int(math.ceil(zooming * height))+10)) 
    

    
    def set_image(self, image):
        self.image=image
        self.show_image()
        
    def change_size(self, event):
        self.size= event.width, event.height
        if self.imageid is not None :
            self.change_format(self.image)

    def wheel(self, event):
        ''' Zoom with mouse wheel '''
        scale = 1.0
        x = self.canvas.canvasx(event.x)
        y = self.canvas.canvasy(event.y)
        if event.num == 5 or event.delta == -120:
            if self.imscale[0]*self.delta < self.minScale[0] or self.imscale[1]*self.delta < self.minScale[1]: #Cannot decrease the scale when we are at min
                return
            else:
                scale        *= self.delta
                self.imscale =  self.imscale[0]*self.delta , self.imscale[1]*self.delta
        if event.num == 4 or event.delta == 120:
            if (self.imscale[0]/self.delta) / self.minScale[0]  > 8 or (self.imscale[1]/self.delta) / self.minScale[1]  > 8: #Limit to 4X for zooming
                return
            else:
                scale        /= self.delta
                self.imscale = self.imscale[0]/self.delta , self.imscale[1]/self.delta
        
        widthi, heighti = self.image.size
        self.canvas.scale('all', 0, 0, scale, scale)
        if self.image is not None:
            self.show_image()
        self.canvas.configure(scrollregion=(0,0,int(self.imscale[0] * widthi), int(self.imscale[1] * heighti)))
        if scale>1:
            self.canvas.xview_moveto((x+ sum(self.xlastview))/int(self.imscale[0] * widthi))
            self.canvas.yview_moveto((y+ sum(self.ylastview))/int(self.imscale[1] * heighti))
            self.xlastview.append(x)
            self.ylastview.append(y)
        elif scale<1:
            self.canvas.xview_moveto(sum(self.xlastview[0:len(self.xlastview)-1])/int(self.imscale[0] * widthi))
            self.canvas.yview_moveto(sum(self.ylastview[0:len(self.ylastview)-1])/int(self.imscale[1] * heighti))
            try:
                self.xlastview.pop()
                self.ylastview.pop()
            except:
                pass


        
    def scroolImage(self,event):
        if event.__dict__['keysym_num'] == 65362: #Up
            self.canvas.yview_scroll(-1,tk.UNITS)
        elif event.__dict__['keysym_num'] == 65364: #Down
            self.canvas.yview_scroll(1,tk.UNITS)
        elif event.__dict__['keysym_num'] == 65361: #Left
            self.canvas.xview_scroll(-1,tk.UNITS)
        elif event.__dict__['keysym_num'] == 65363: #Rigth
            self.canvas.xview_scroll(1,tk.UNITS)

    def show_image(self):
        ''' Show image on the Canvas '''
        if self.imageid:
            self.canvas.delete(self.imageid)
            self.imageid = None
            self.canvas.imagetk = None  
        width, height = self.image.size
        new_size = int(self.imscale[0] * width), int(self.imscale[1] * height)
        imagetk = ImageTk.PhotoImage(self.image.resize(new_size))
        # Use self.text object to set proper coordinates
        self.imageid = self.canvas.create_image(0,0,
                                                anchor='nw', image=imagetk)
    
        self.canvas.lower(self.imageid)  
        self.canvas.imagetk = imagetk  # keep an extra reference to prevent garbage-collection



