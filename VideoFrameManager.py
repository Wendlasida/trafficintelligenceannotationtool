#Video Gestions Class
import cv2
import math
from trafficintelligence import moving, cvutils
import tkinter.messagebox as tkMessageBox

class VideoFrameManager:
    def __init__(self, VideoTool, video_source=0, boundingBoxes = {}, undistort = False, intrinsicCameraMatrix = None, distortionCoefficients = None, undistortedImageMultiplication = 1., annotations = [], gtMatches = {}, toMatches = {}, colorBlind = False):
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            tkMessageBox.showerror("Video source error", "Unable to open video source '{}'".format(video_source))
            raise ValueError("Unable to open video source", video_source)
        self.VideoTool=VideoTool
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
        self.boundingBoxes=boundingBoxes
        self.undistort=undistort
        self.annotations=annotations
        self.toMatches=toMatches
        self.map1=None
        self.map2=None
        self.newCameraMatrix=None
        self.gtMatches=gtMatches
        self.colorBlind=colorBlind
        if undistort: # setup undistortion
            [self.map1, self.map2], self.newCameraMatrix = cvutils.computeUndistortMaps(self.width, self.height, undistortedImageMultiplication, intrinsicCameraMatrix, distortionCoefficients)
    
    
    def updateTransformationParams(self, undistort = None, intrinsicCameraMatrix = None, distortionCoefficients = None, undistortedImageMultiplication = None):
        for obj in self.VideoTool.objectList[:]:
            if hasattr(obj, 'projectedPositions'):
                delattr(obj, 'projectedPositions')
        self.undistort = undistort if undistort is not None else self.undistort
        if self.undistort and undistortedImageMultiplication is not None and intrinsicCameraMatrix is not None and distortionCoefficients is not None :
            [self.map1, self.map2], self.newCameraMatrix = cvutils.computeUndistortMaps(self.width, self.height, undistortedImageMultiplication, intrinsicCameraMatrix, distortionCoefficients)
    
    
    def getConfiguredFrame(self,Next=True):
        '''Displays the objects overlaid frame by frame over the video '''
        if self.colorBlind:
            colorType = 'colorblind'
        else:
            colorType = 'default'
        frameNum=self.VideoTool.frameNumber-1
        if not Next:
            frameNum = -1 if frameNum==-1 else frameNum-1
            self.vid.set(cv2.CAP_PROP_POS_FRAMES,frameNum)
        ret, img = self.get_frame()
        if ret:
            for obj in self.VideoTool.objectList[:]:
                color = tuple(int(self.VideoTool.getTheObjectColor(obj).lstrip('#')[i:i+2], 16) for i in (0, 2 ,4))
                if obj.existsAtInstant(frameNum):
                    if not hasattr(obj, 'projectedPositions'):
                        obj.projectedPositions = obj.getPositions().homographyProject(self.VideoTool.invHomography).newCameraProject(self.newCameraMatrix)
                    cvutils.cvPlot(img, obj.projectedPositions, color, frameNum-obj.getFirstInstant(), thickness = int(math.ceil(self.VideoTool.PlotThickness/min(self.VideoTool.Zoomable.minScale))))
                    objDescription = ''
                    if moving.userTypeNames[obj.userType] != 'unknown':
                        objDescription += moving.userTypeNames[obj.userType][0].upper()
                    if len(self.annotations) > 0: # if we loaded annotations, but there is no match
                        if frameNum not in self.toMatches[obj.getNum()]:
                            objDescription += " FA"
                    try : 
                        cv2.putText(img, objDescription, obj.projectedPositions[frameNum-obj.getFirstInstant()].asint().astuple(), cv2.FONT_HERSHEY_PLAIN, 4, color, thickness = int(math.ceil(self.VideoTool.PlotThickness/min(self.VideoTool.Zoomable.minScale))) )
                    except Exception as e:
                        print("Object #{} at frame {}, exists from {} to {}, trying to get the value no {} for projected object whose length is {}".format(obj.getNum(), frameNum, obj.getFirstInstant(), obj.getLastInstant(), frameNum-obj.getFirstInstant(), len(obj.projectedPositions)))
                        print('Exception error : ', str(e))
            # plot object bounding boxes
            if frameNum in self.boundingBoxes.keys():
                for rect in self.boundingBoxes[frameNum]:
                    cv2.rectangle(img, rect[0].asint().astuple().homographyProject(self.VideoTool.invHomography), rect[1].asint().astuple().homographyProject(self.VideoTool.invHomography), color)
            # plot ground truth
            if len(self.annotations) > 0:
                for gt in self.annotations:
                    if gt.existsAtInstant(frameNum):
                        if not frameNum in self.gtMatches[gt.getNum()]:
                            cv2.putText(img, 'Miss', gt.topLeftPositions[frameNum-gt.getFirstInstant()].asint().astuple(), cv2.FONT_HERSHEY_PLAIN, 1, color)
                        cv2.rectangle(img, gt.topLeftPositions[frameNum-gt.getFirstInstant()].asint().astuple(), gt.bottomRightPositions[frameNum-gt.getFirstInstant()].asint().astuple(), color)
            return ret, img                    
        else:
            return (False , None)
        
            
            
            

    #Returns the current frame
    def get_frame(self, Next=True, Tracker = None):
        if self.vid.isOpened():
            if not Next:
                self.vid.set(cv2.CAP_PROP_POS_FRAMES, self.VideoTool.frameNumber-1)
            ret, frame = self.vid.read()
            if ret:
                if self.undistort:
                    frame = cv2.remap(frame, self.map1, self.map2, interpolation=cv2.INTER_LINEAR)
                if Tracker is not None:
                    frame = Tracker.update(frame)
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        return (False, None)
    
    #Returns the number of sec between 2 frames
    def get_delay_between_frame(self):
        if self.vid.isOpened():
            return int(self.vid.get(cv2.CAP_PROP_FPS))
        return None
    
    #Get the number of frame in a video
    def get_max_frame_pos(self):
        if self.vid.isOpened():
            return int(self.vid.get(cv2.CAP_PROP_FRAME_COUNT))
        return None
    
    #Set a specific frame
    def set_to_specific_frame(self,pos):
        if self.vid.isOpened():
            try :
                self.vid.set(cv2.CAP_PROP_POS_FRAMES,pos-1)
            except :
                pass
    
    #Gets the current
    def get_current_frame_pos(self):
        if self.vid.isOpened():
            return int(self.vid.get(cv2.CAP_PROP_POS_FRAMES))
        return None
    
    #Returns True if it's not a video on streaming
    def is_frame_pos_limited(self):
        if self.vid.isOpened():
            return int(self.vid.get(cv2.CAP_PROP_POS_FRAMES))!=-1
        return False
    
    #Get the previous fame
    def get_previous_frame(self):
        if self.vid.isOpened():
            try :
                current_pos=self.VideoTool.frameNumber
                if current_pos>1 :
                    self.vid.set(cv2.CAP_PROP_POS_FRAMES,current_pos-2)
                    ret, frame = self.get_frame()
                    if ret:
                        return (ret, frame)
                    else:
                        return (ret, None)
            except:
                pass
        return (ret, None)
            
    #Get the previous fame
    def get_previous_configured_frame(self):
        if self.vid.isOpened():
            try :
                current_pos=self.vid.get(cv2.CAP_PROP_POS_FRAMES)
                if current_pos>1 :
                    self.vid.set(cv2.CAP_PROP_POS_FRAMES,current_pos-2)
                    ret, frame = self.getConfiguredFrame()
                    if ret:
                        return (ret, frame)
                    else:
                        return (ret, None)
            except:
                pass
        return (ret, None)

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()
